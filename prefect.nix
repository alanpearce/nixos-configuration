{ config, pkgs, ... }:

{ imports = [
    ./hardware-configuration.nix

    ./modules/satoshipay.nix
    ./modules/base.nix
    ./modules/configuration/berlin.nix
    ./modules/configuration/british-english.nix
    ./modules/configuration/nix.nix
    ./modules/configuration/user.nix
    ./modules/development/base.nix
    ./modules/development/javascript.nix
    ./modules/development/lisp.nix
    ./modules/hardware/audio.nix
    ./modules/hardware/bare-metal.nix
    ./modules/hardware/mouse.nix
    ./modules/hardware/systemd-boot.nix
    ./modules/hardware/nvidia-gpu.nix
    ./modules/hardware/trezor.nix
    ./modules/services/zeroconf.nix
    ./modules/user-interface.nix
    ./modules/programs/accounting.nix
    ./modules/programs/dotfiles.nix
    ./modules/programs/gnupg.nix
    ./modules/programs/keybase.nix
    ./modules/programs/i3.nix
    ./modules/programs/infrastructure.nix
    ./modules/programs/passwords.nix
    ./modules/programs/shell.nix
    ./modules/gaming.nix
    ./modules/programs/tor.nix
    ./modules/programs/barrier.nix
    ./modules/services/syncthing.nix
  ];
  boot.loader.efi.canTouchEfiVariables = pkgs.lib.mkForce false;
  services.xserver.screenSection = ''
    Option "MetaModes" "2560x1440_120"
  '';

  services.xserver.xautolock.enable = pkgs.lib.mkForce false;
  powerManagement.enable = false;
  # powerManagement.cpuFreqGovernor = "performance";
  networking = {
    hostName = "prefect";
    domain = "alanpearce.eu";
  };

  system.stateVersion = "19.03";
}
