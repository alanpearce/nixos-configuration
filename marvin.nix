{ config, pkgs, ... }:

{ imports = [
    ./hardware-configuration.nix

    ./modules/base.nix
    ./modules/configuration/berlin.nix
    ./modules/configuration/british-english.nix
    ./modules/configuration/nix.nix
    ./modules/configuration/user.nix
    ./modules/development/base.nix
    ./modules/development/javascript.nix
    ./modules/development/lisp.nix
    ./modules/hardware/adb.nix
    ./modules/hardware/audio.nix
    ./modules/hardware/qwerty.nix
    ./modules/hardware/network-manager.nix
    ./modules/hardware/nitrokey.nix
    ./modules/hardware/systemd-boot.nix
    ./modules/hardware/trackball.nix
    ./modules/hardware/trezor.nix
    ./modules/machines/x250.nix
    ./modules/programs/accounting.nix
    ./modules/programs/dotfiles.nix
    ./modules/programs/i3.nix
    ./modules/programs/infrastructure.nix
    ./modules/programs/passwords.nix
    ./modules/programs/shell.nix
    ./modules/services/syncthing.nix
  ];

  networking = {
    hostName = "marvin";
    domain = "alanpearce.eu";
  };

  system.stateVersion = "18.09";
}
