{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./private/default.nix

    ./modules/base.nix
    ./modules/configuration/berlin.nix
    ./modules/configuration/british-english.nix
    ./modules/configuration/nix.nix
    ./modules/configuration/user.nix
    ./modules/development/base.nix
    ./modules/development/javascript.nix
    ./modules/hardware/audio.nix
    ./modules/hardware/systemd-boot.nix
    ./modules/hardware/keyboardio-model01.nix
    ./modules/hardware/network-manager.nix
    ./modules/hardware/qwerty.nix
    ./modules/hardware/trackball.nix
    ./modules/hardware/trezor.nix
    ./modules/machines/t470s.nix
    ./modules/programs/dotfiles.nix
    ./modules/programs/gnupg.nix
    ./modules/programs/keybase.nix
    ./modules/programs/tor.nix
    ./modules/programs/i3.nix
    ./modules/programs/infrastructure.nix
    ./modules/programs/passwords.nix
    ./modules/programs/shell.nix
    ./modules/satoshipay.nix
  ];

  boot.initrd.luks.devices = [
    {
      name = "root";
      allowDiscards = true;
      device = "/dev/disk/by-uuid/bb7f9a24-8963-4d00-9258-118050b35748";
      preLVM = true;
    }
  ];

  networking.hostName = "satoshipad";

  nix.gc.dates = "12:30";
  system.autoUpgrade.dates = "13:24";
  virtualisation.docker.autoPrune.dates = "Mon, 13:00";

  system.stateVersion = "18.03";

  systemd.user.services.mbsync = {
    description = "Mailbox synchronisation";
    path = with pkgs; [ python3Packages.keyring ];
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${pkgs.isync}/bin/mbsync -Va";
      ExecStartPost = "${pkgs.myEmacs}/bin/emacsclient -e (mu4e-update-index)";
    };
  };

  systemd.user.timers.mbsync = {
    description = "Mailbox synchronisation timer";
    wantedBy = [ "default.target" ];
    timerConfig = {
      OnBootSec = "2m";
      OnUnitActiveSec = "10m";
    };
  };
}
