{ config, pkgs, ... }:

{ boot.loader = {
    grub = {
      enable = true;
      splashImage = null;
      version = 2;
      device = "nodev";
      efiSupport = true;
    };
    efi.canTouchEfiVariables = true;
  };
}
