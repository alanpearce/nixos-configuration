{ config, pkgs, lib, ... }:

{ services.printing.enable = true;
  systemd.services.cups.wantedBy = lib.mkForce [];
  systemd.sockets.cups.wantedBy = [ "sockets.target" ];
  systemd.services.cups-browsed.wantedBy = lib.mkForce [];

  systemd.timers.cups-browsed = {
    description = "Delayed startup of CUPS Remote Printer Discovery";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "2 min";
    };
  };

  imports = [
    ../services/zeroconf.nix
  ];
}
