{ config, pkgs, ... }:

{ programs.adb.enable = true;
  users.groups.adbusers = {};

  services.udev = {
   packages = [ pkgs.android-udev-rules ];
  };
}
