{ config, pkgs, ... }:

{ services.xserver.videoDrivers = [ "nvidia" ];
  nixpkgs.config.allowUnfree = true;
}
