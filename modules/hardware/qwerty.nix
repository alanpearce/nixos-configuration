{ config, pkgs, ... }:

{ services.kmscon = {
    extraConfig = ''
      xkb-model=pc104
      xkb-layout=us
      xkb-variant=intl-unicode
      xkb-options="altwin:prtsc_rwin"
    '';
  };
  services.xserver = {
    layout = "us";
    xkbVariant = "intl-unicode";
    xkbOptions = "altwin:prtsc_rwin";
  };
}
