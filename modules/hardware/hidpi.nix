{ config, pkgs, ... }:

{ i18n = {
    consoleFont = "ter-v24b";
    consolePackages = with pkgs; [
      terminus_font
    ];
  };
}
