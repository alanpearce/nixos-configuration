{ config, pkgs, ... }:

{ hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    vaapiVdpau
    libvdpau-va-gl
  ];

  services.xserver.videoDrivers = [ "intel" "modesetting" ];

  boot.earlyVconsoleSetup = true;
  boot.initrd.kernelModules = [
    "i915"
  ];
}
