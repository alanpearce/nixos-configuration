{ config, pkgs, lib, ... }:

{ boot.kernelModules = [ "coretemp" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [
    x86_energy_perf_policy
  ];

  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = false;
      package = pkgs.bluezFull;
    };
    pulseaudio = {
      extraModules = with pkgs; [
        pulseaudio-modules-bt
      ];
    };
  };
  systemd.services.bluetooth.wantedBy = lib.mkForce [];
  systemd.timers.bluetooth = {
    description = "Delayed startup of Bluetooth";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "1 min";
    };
  };

  environment.systemPackages = with pkgs; [
    blueman
    bluez-tools

    powerstat
    powertop

    arandr
    autorandr
    disper
  ];

  programs.light.enable = true;

  services.autorandr.enable = true;

  services.logind.extraConfig = ''
    IdleAction=suspend
    IdleActionSec=600
  '';

  services.acpid = {
    enable = true;
    lidEventCommands = ''
      ${pkgs.autorandr}/bin/autorandr --batch --change
    '';
  };

  services.tlp = {
    enable = true;
    extraConfig = ''
      CPU_SCALING_GOVERNOR_ON_BAT=powersave
      ENERGY_PERF_POLICY_ON_BAT="balance_power"

      SOUND_POWER_SAVE_ON_AC=60
      DEVICES_TO_DISABLE_ON_BAT_NOT_IN_USE="bluetooth wwan"
    '';
  };

  services.xserver = {
    libinput = {
      enable = lib.mkDefault true;
      naturalScrolling = true;
      disableWhileTyping = true;
    };
    displayManager.sessionCommands = ''
      ${pkgs.autorandr}/bin/autorandr --change --force
      ${pkgs.blueman}/bin/blueman-applet &
    '';
  };

  systemd.services.nixos-upgrade.unitConfig.ConditionACPower = true;
  systemd.services.nix-gc.unitConfig.ConditionACPower = true;
  systemd.services.docker-prune.unitConfig.ConditionACPower = true;

  imports = [
    ../user-interface.nix
  ];
}
