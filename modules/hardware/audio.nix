{ config, pkgs, ... }:

{ hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
    daemon.config = {
      flat-volumes = "no";
    };
    package = if config.hardware.bluetooth.enable
      then pkgs.pulseaudioFull
      else pkgs.pulseaudio;
  };

  sound.enable = true;

  environment.systemPackages = with pkgs; [
    pamixer
    pavucontrol
  ];
}
