{ config, pkgs, ... }:

{ networking.connman = {
    enable = true;
    enableVPN = false;
  };
  networking.wireless.enable = true;

  environment.systemPackages = with pkgs; [
    cmst
    connman-notify
    connman_dmenu
  ];
}
