{ config, pkgs, ... }:

{ services.xserver = {
    libinput.enable = false;
    synaptics = {
      enable = true;

      accelFactor = "0.04";

      minSpeed = "0.3";
      maxSpeed = "0.6";

      palmDetect = true;
      palmMinWidth = 5;
      palmMinZ = 20;

      twoFingerScroll = true;
      vertTwoFingerScroll = true;
      horizTwoFingerScroll = true;
      additionalOptions = ''
        Option "RBCornerButton" "3"
        Option "VertScrollDelta" "-111"
        Option "HorizScrollDelta" "-111"
      '';
    };
  };
}
