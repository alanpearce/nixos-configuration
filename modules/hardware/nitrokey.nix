{ config, pkgs, lib, ... }:

{
  hardware.nitrokey = {
    enable = true;
  };

  services.pcscd.enable = true;

  environment.systemPackages = with pkgs; [
    nitrokey-app
  ];
}
