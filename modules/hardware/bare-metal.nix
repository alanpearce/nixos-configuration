{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    fuse_exfat
    cryptsetup
    dmidecode
    hdparm
    pciutils
    usbutils
  ];

  hardware.cpu.intel.updateMicrocode = true;

  boot.kernel.sysctl = {
    "net.ipv4.tcp_allowed_congestion_control" = "illinois reno lp";
    "net.ipv4.tcp_congestion_control" = "illinois";
  };

  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };
  boot.tmpOnTmpfs = true;

  boot.kernelModules = [ "bfq" ];

  fileSystems."/".options = [ "noatime" "nodiratime" ];
  fileSystems."/home".options = [ "noatime" "nodiratime" ];
}
