{ config, lib, pkgs, ... }:

{ services.trezord.enable = true;
  environment.systemPackages = with pkgs; [
    gnupg
    pinentry
    (python3.withPackages(ps: with ps; [ trezor_agent wheel ]))
  ];
  programs.gnupg.agent = {
    enable = lib.mkForce false;
    enableSSHSupport = lib.mkForce false;
  };
}
