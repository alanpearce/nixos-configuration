{ config, pkgs, ... }:

{ services.xserver.config = ''
    Section "InputClass"
        Identifier "Mouse (No Acceleration)"
        MatchIsPointer "yes"
        MatchIsTouchpad "no"
        Option "AccelerationProfile" "-1"
        Option "AccelerationScheme" "none"
    EndSection
  '';
}
