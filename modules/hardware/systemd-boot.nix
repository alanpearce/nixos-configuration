{ config, pkgs, ... }:

{ boot.loader.systemd-boot = {
    enable = true;
    editor = false; # Don't allow modification
  };
  boot.loader.efi.canTouchEfiVariables = true;
  boot.vesa = true;
  boot.earlyVconsoleSetup = true;
}
