{ config, pkgs, ... }:

{ networking.networkmanager = {
    enable = true;
    dns = "unbound";
  };

  environment.systemPackages = with pkgs; [
    networkmanagerapplet
    networkmanager_dmenu
  ];
}
