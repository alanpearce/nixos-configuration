{ config, pkgs, ... }:

{ services.xserver.config = ''
    Section "InputClass"
        Identifier "Trackball (No Acceleration)"
        MatchIsPointer "yes"
        MatchIsTouchpad "no"
        MatchProduct "Trackball"
        Option "AccelerationProfile" "-1"
        Option "AccelerationScheme" "none"
    EndSection
  '';
}
