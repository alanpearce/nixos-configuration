{ config, pkgs, ... }:

{ boot.kernelModules = [ ];
  boot.blacklistedKernelModules = [ "thinkpad_ec" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [
    acpi_call
  ];

  hardware.trackpoint = {
    enable = true;
    emulateWheel = true;
  };

  services.thinkfan = {
    enable = true;
  };

  services.tlp = {
    enable = true;
  };

  imports = [
    ./bare-metal.nix
    ./laptop.nix
  ];
}
