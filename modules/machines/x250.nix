{ config, pkgs, ... }:

{ boot.extraModulePackages = with config.boot.kernelPackages; [
    acpi_call
  ];

  hardware.firmware = with pkgs; [
    firmwareLinuxNonfree
  ];

  services.tlp.extraConfig = ''
    # Newer Thinkpads have a battery firmware
    # it conflicts with TLP if stop thresholds are set
    START_CHARGE_THRESH_BAT0=70
    # STOP_CHARGE_THRESH_BAT0=80
    START_CHARGE_THRESH_BAT1=70
    # STOP_CHARGE_THRESH_BAT1=80

    DISK_APM_LEVEL_ON_AC="254 254"
    DISK_APM_LEVEL_ON_BAT="128 128"

    # One or both of these lines stops disk corruption
    # when re-attaching to AC whilst on.
    SATA_LINKPWR_ON_BAT=medium_power
    SATA_LINKPWR_BLACKLIST="host1"
  '';

  imports = [
    ../hardware/synaptics.nix
    ../hardware/intel-gpu.nix
    ../hardware/thinkpad.nix
  ];
}
