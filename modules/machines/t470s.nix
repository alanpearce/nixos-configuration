{ config, pkgs, ... }:

{ hardware.usbWwan.enable = false; # unused
  systemd.services.ModemManager.enable = false;

  hardware.firmware = with pkgs; [
    firmwareLinuxNonfree
  ];

  services.thinkfan.sensors = ''
    hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon0/temp3_input
    hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon0/temp1_input
    hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon0/temp2_input
  '';
  services.thinkfan.levels = ''
    (0,     0,      48)
    (1,     45,     52)
    (2,     50,     57)
    (3,     55,     63)
    (6,     60,     65)
    (7,     60,     85)
    (127,   80,     32767)
  '';

  boot.kernelParams = [
    "i915.enable_guc=2"
    "i915.enable_psr=1"
  ];

  boot.postBootCommands = ''
    echo bfq > /sys/block/nvme0n1/queue/scheduler
  '';

  hardware.pulseaudio.extraConfig = ''
    load-module module-alsa-sink device=hw:0,7
  '';

  services.tlp.extraConfig = ''
    DISK_DEVICES="nvme0n1"
    DISK_IOSCHED="keep"
  '';

  services.xserver = {
    dpi = 109;
    monitorSection = ''
      DisplaySize 310 176
    '';
  };

  environment.systemPackages = with pkgs; [
    nvme-cli
  ];

  imports = [
    ../hardware/intel-gpu.nix
    ../hardware/hidpi.nix
    ../hardware/thinkpad.nix
  ];
}
