{ config, pkgs, lib, ... }:

{
  services.tor = {
    enable = true;
    client = {
      enable = true;
      socksListenAddress = "9050 IPv6Traffic";
    };
    torsocks = {
      enable = true;
    };
  };
  systemd.services.tor.wantedBy = lib.mkForce [];
  systemd.timers.tor = {
    description = "Delayed startup of Tor";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "1 min";
    };
  };
}
