{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    hugo

    nixops
  ];
}
