{ config, pkgs, ... }:

{ services.xserver.windowManager = {
    default = "i3";
    i3 = {
      enable = true;
      extraSessionCommands = ''
        ${pkgs.dunst}/bin/dunst &
        ${pkgs.sxhkd}/bin/sxhkd &
      '';
    };
  };

  environment.systemPackages = with pkgs; [
    i3status
  ];

  imports = [
    ./window-manager.nix
  ];
}
