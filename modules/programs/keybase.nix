{ config, pkgs, lib, ... }:

{
  services.keybase.enable = true;
  services.kbfs.enable = true;
  environment.variables.NIX_SKIP_KEYBASE_CHECKS = "1";

  environment.systemPackages = with pkgs; [
    keybase-gui
  ];
}
