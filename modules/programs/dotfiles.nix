{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    stow
    fzf
    ghq
  ] ++ (
    if stdenv.isDarwin
    then [
    ] else [
      git
    ]
  );
}
