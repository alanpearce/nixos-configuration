{ config, pkgs, ... }:

{ services.xserver = {
   desktopManager.xterm.enable = false;

   displayManager = {
     lightdm = {
       enable = true;
       greeters.mini = {
         enable = true;
         user = "alan";
       };
     };
     sessionCommands = ''
       ${pkgs.xorg.xrdb}/bin/xrdb -merge $HOME/.xresources/main
       ${pkgs.xorg.xsetroot}/bin/xsetroot -cursor_name left_ptr -solid '#4d4d4c'
     '';
   };
    xautolock = {
      enable = true;
      locker = "${pkgs.i3lock}/bin/i3lock -n";
      enableNotifier = true;
      notifier = "${pkgs.libnotify}/bin/notify-send \"Locking in 10 seconds\"";
      time = 5;
    };
  };

  services.xserver.displayManager.setupCommands = ''
    ${pkgs.redshift}/bin/redshift \
      -l ${toString config.services.redshift.latitude}:${toString config.services.redshift.longitude} \
      -t ${toString config.services.redshift.temperature.day}:${toString config.services.redshift.temperature.night} \
      -b 1:1 \
      -o \
      -r \
  '';

  environment.systemPackages = with pkgs; [
    dmenu
    dunst
    libnotify # for notify-send
    rofi
    sxhkd
    maim

    perlPackages.FileMimeInfo # xdg-utils uses this when no DE
  ];
}
