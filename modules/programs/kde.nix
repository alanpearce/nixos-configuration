{ config, lib, pkgs, ... }:

with lib;
{ services = {
    xserver = {
      desktopManager = {
        plasma5.enable = true;
      };
      displayManager = {
        sddm.enable = true;
      };
    };

    physlock.enable = lib.mkForce false;
  };

  environment.systemPackages = with pkgs; [
    kde-gtk-config
  ];
}
