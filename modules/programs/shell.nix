{ config, pkgs, ... }:

{ programs.zsh = {
    enable = true;
    promptInit = "";
  };
  programs.bash.enableCompletion = true;
  programs.vim = pkgs.lib.attrsets.optionalAttrs pkgs.stdenv.isDarwin {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    pv
    fd
    unstable.sd
    entr
    file
    htop
    lsof
    iftop
    nmap
    moreutils
    mtr
    tree
    zip
    telnet
  ] ++ (
  if !stdenv.isDarwin
  then [
    vim
    unar
  ] else [
  ]);
}
