{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    gnupg
    pinentry
    (python3.withPackages(ps: with ps; [ trezor_agent wheel ]))
  ];
  environment.variables.GNUPGHOME = "$HOME/.gnupg/trezor/";
}
