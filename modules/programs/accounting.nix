{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    ledger
    bean-add
    beancount
    fava
    reckon
  ];
}
