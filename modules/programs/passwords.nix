{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs.unstable; [
    keepassx-community
    rofi-pass
    pass
    pwgen
  ];
}
