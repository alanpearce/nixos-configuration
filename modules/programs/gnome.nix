{ config, lib, pkgs, ... }:

with lib;
{ services = {
    gnome3 = {
      gnome-documents.enable = false;
      gnome-user-share.enable = false;
      gnome-online-accounts.enable = false;
      seahorse.enable = false;
      tracker.enable = false;
    };
    telepathy.enable = false;

    xserver = {
      desktopManager.gnome3 = {
        enable = true;
        extraGSettingsOverrides = ''
          [org.gnome.desktop.input-sources]
          sources=[('xkb','${config.services.xserver.layout + (optionalString (config.services.xserver.xkbVariant != "") ("+" + config.services.xserver.xkbVariant))}')]
        '';
      };
    };
  };
}
