{ config, pkgs, ... }:

{ nix = {
    buildCores = 0;

    daemonNiceLevel = 19;
    daemonIONiceLevel = 7;

    autoOptimiseStore = true;
    gc = {
      automatic = true;
      options = "--delete-older-than 14d";
    };
  };

  nixpkgs.config = {
    packageOverrides = pkgs: {
      unstable = import <nixos-unstable> {
        config = config.nixpkgs.config;
      };
    };
  };

  environment.systemPackages = with pkgs; [
    cachix
  ];

  system.autoUpgrade = {
    enable = true;
    flags = [ "--max-jobs" "1" ];
  };
  systemd.services.nixos-upgrade = {
    script = pkgs.lib.mkForce ''
      ${pkgs.nix}/bin/nix-channel --update nixos-unstable
      ${config.system.build.nixos-rebuild}/bin/nixos-rebuild boot --no-build-output --upgrade ${toString config.system.autoUpgrade.flags}
    '';
  };
}
