{ config, pkgs, ... }:

{ time.timeZone = "Europe/Berlin";
  services.redshift = {
    latitude = "52.586";
    longitude = "13.300";
  };
}
