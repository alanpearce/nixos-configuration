{ config, pkgs, ... }:

{ time.timeZone = "Europe/London";
  services.redshift = {
    latitude = "52.2394";
    longitude = "-0.9416";
  };
}
