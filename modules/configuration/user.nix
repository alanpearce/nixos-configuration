{ config, pkgs, ... }:

{ users.extraUsers.alan = {
    description = "Alan Pearce";
    isNormalUser = true;
    extraGroups = [ "audio" "wheel" "lp" "adbusers" "docker" "nitrokey" "dialout" "networkmanager" ];
    shell = "/run/current-system/sw/bin/zsh";
    home = "/home/alan";
    uid = 1000;
  };
}
