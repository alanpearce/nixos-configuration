{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    unstable.steam
    (
      unstable.winePackages.unstable.override {
        pngSupport = true;
        jpegSupport = true;
        tiffSupport = true;
        gettextSupport = true;
        fontconfigSupport = true;
        alsaSupport = true;
        gtkSupport = true;
        openglSupport = true;
        tlsSupport = true;
        gstreamerSupport = true;
        cupsSupport = true;
        colorManagementSupport = true;
        dbusSupport = true;
        mpg123Support = true;
        openalSupport = true;
        openclSupport = true;
        cairoSupport = true;
        odbcSupport = true;
        netapiSupport = true;
        cursesSupport = true;
        vaSupport = true;
        pcapSupport = true;
        v4lSupport = true;
        saneSupport = true;
        gsmSupport = true;
        gphoto2Support = true;
        ldapSupport = true;
        pulseaudioSupport = true;
        udevSupport = true;
        xineramaSupport = true;
        xmlSupport = true;
        vulkanSupport = true;
        sdlSupport = true;
      }
    )
    unstable.lutris
  ];
  hardware.steam-hardware.enable = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;

  systemd = {
    extraConfig = ''
      DefaultLimitNOFILE=524288
    '';
    user.extraConfig = ''
      DefaultLimitNOFILE=524288
    '';
  };

  networking.firewall = {
    allowedUDPPorts = [
      27031
      27036
    ];
    allowedTCPPorts = [
      27036
      27037
    ];
  };
}
