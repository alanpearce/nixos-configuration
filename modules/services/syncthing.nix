{ config, pkgs, ... }:

{ services.syncthing = {
    enable = true;
    user = "alan";
    group = "users";
    openDefaultPorts = true;
    systemService = true;
    dataDir = "/home/alan/.config/syncthing";
  };
}
