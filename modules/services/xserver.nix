{ config, pkgs, ... }:

{ services.xserver = {
    enable = true;
    enableCtrlAltBackspace = true;
    exportConfiguration = true;
  };

  environment.systemPackages = with pkgs; [
    xorg.xmodmap
    xorg.xinit
    xorg.xev
    xorg.xdpyinfo
    xclip
    xfontsel

    vanilla-dmz
    capitaine-cursors
    bibata-cursors

    arc-theme
    hicolor_icon_theme
    paper-gtk-theme
    paper-icon-theme

    arc-icon-theme
    tango-icon-theme

    gtk-engine-murrine
    gtk_engines
  ];

  fonts = {
    enableFontDir = true;
    enableDefaultFonts = false;
    fontconfig = {
      defaultFonts = {
        monospace = [ "Liberation Mono" ];
        sansSerif = [ "Liberation Sans" ];
        serif = [ "Liberation Serif" ];
      };
      penultimate = {
        enable = true;
      };
      ultimate = {
        enable = false;
        preset = "osx";
      };
    };
    fonts = with pkgs; [
      dina-font
      envypn-font
      profont
      proggyfonts
      terminus_font

      fantasque-sans-mono
      emacs-all-the-icons-fonts
      fira
      fira-code
      fira-mono
      go-font
      font-awesome_5
      ibm-plex
      liberation_ttf
      mononoki
      roboto
      roboto-mono
      roboto-slab
      source-code-pro
      source-sans-pro
      source-serif-pro
      xorg.fontmiscmisc
      xorg.fontcursormisc
      xorg.fontbhlucidatypewriter100dpi
    ];
  };
}
