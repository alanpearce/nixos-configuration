{ config, pkgs, lib, ... }:

{ services.avahi = {
    enable = true;
    nssmdns = true;
    ipv6 = true;
  };
  systemd.services.avahi-daemon.wantedBy = lib.mkForce [];
  systemd.timers.avahi-daemon = {
    description = "Delayed startup of Avahi";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "1 min";
    };
  };
}
