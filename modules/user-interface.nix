{ config, pkgs, lib, makeDesktopItem, ... }:

let
  emacsPackage = import ../packages/emacs.nix {
    pkgs = pkgs.unstable;
    emacs = pkgs.unstable.emacs.override {
      withGTK3 = false;
    };
  };
  editorScript = pkgs.writeScriptBin "edit" ''
    #!${pkgs.runtimeShell}
    if [ -z "$1" ]; then
      exec ${emacsPackage}/bin/emacsclient --create-frame --alternate-editor ${emacsPackage}/bin/emacs
    else
      exec ${emacsPackage}/bin/emacsclient --alternate-editor ${emacsPackage}/bin/emacs "$@"
    fi
  '';
  desktopApplicationFile = makeDesktopItem {
    name = "emacsclient.desktop";
    destination = "/share/applications/emacsclient.desktop";
    text = ''
      [Desktop Entry]
      Name=Emacsclient
      GenericName=Text Editor
      Comment=Edit text
      MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
      Exec=${editorScript}/bin/edit %F
      Icon=emacs
      Type=Application
      Terminal=false
      Categories=Development;TextEditor;
      StartupWMClass=Emacs
      Keywords=Text;Editor;
    '';
  };
in
{ documentation.info.enable = true;
  nixpkgs.config.firefox.enableOfficialBranding = true;

  nixpkgs.config.packageOverrides = pkgs: {
    myEmacs = emacsPackage;
  };
  environment.systemPackages = with pkgs; [
    aria2
    firefox
    pcmanfm

    epdfview
    geeqie

    cmus

    fish # for emacs-fish-completion
    myEmacs
    editorScript

    unstable.xst # st, but with support for XResources

    lxappearance
    lxrandr
    lxtask

    python3Packages.keyring
    isync
    msmtp
    html2text

    weechat

    unstable.pass-otp

    mpv

    mosh
    aspell
    aspellDicts.en

    cifs-utils
    hexchat
    signal-desktop
    wire-desktop

    trash-cli
  ] ++ (if !stdenv.isDarwin
  then [
    unstable.mu
  ]
  else []);

  nixpkgs.config.allowUnfree = true;

  services.compton = {
    enable = true;
  };

  services.devmon.enable = true;

  environment.sessionVariables.TERMINAL = "st";

  systemd.user.services.trash-clean = {
    path = with pkgs; [ trash-cli ];
    description = "Remove old files from FreeDesktop.org trash";

    serviceConfig = {
      Type = "oneshot";
    };
    script = "trash-empty 30";
  };
  systemd.user.timers.trash-clean = {
    wantedBy = [ "default.target" ];
    timerConfig = {
      OnCalendar = "weekly";
      Persistent = true;
    };
  };

  environment.variables = {
    # This is required so that GTK applications launched from Emacs
    # get properly themed:
    GTK_DATA_PREFIX = "${config.system.path}";
    EDITOR = lib.mkOverride 900 "${editorScript}/bin/edit";
  };

  services.redshift = {
    enable = true;
    temperature = {
      day = 6500;
      night = 3600;
    };
  };

  programs.ssh.startAgent = true;

  programs.dconf.enable = true;
  services.gnome3 = {
    gnome-keyring.enable = true;
    seahorse.enable = true;
    at-spi2-core.enable = true;
  };

  imports = [
    ./services/xserver.nix
  ];
}
