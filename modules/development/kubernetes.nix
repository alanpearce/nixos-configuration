{ config, pkgs, ... }:

{
  networking.bridges = {
    cbr0.interfaces = [];
  };
  networking.interfaces = {
    cbr0 = {
      ipv4.addresses = [
      { address = "10.10.0.1";
        prefixLength = 24;
      }
      ];
    };
  };
  services.kubernetes.roles = ["master" "node"];
  services.kubernetes.kubelet.extraOpts = "--fail-swap-on=false";
  virtualisation.docker.extraOptions = ''
    --iptables=false --ip-masq=false -b cbr0
  '';
}
