{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    ccl
    sbcl
    lispPackages.quicklisp
    asdf
    cl-launch

    dust
    pixie
  ];
}
