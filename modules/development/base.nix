{ config, pkgs, ... }:

{ environment.systemPackages = with pkgs; [
    gitAndTools.gitFull
    gitAndTools.git-extras
    git-lfs

    checkbashisms
    editorconfig-core-c
    go

    wrk

    ag
    (ripgrep.override { withPCRE2 = true; })

    httpie
    jq

    discount
  ] ++ (
    if !stdenv.isDarwin
    then [
      whois
      ldns
      httping
      http-prompt
      firefox-devedition-bin
    ] else [
    ]
  );
}
