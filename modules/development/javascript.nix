{ config, pkgs, ... }:

let
  node = pkgs.unstable.nodejs-10_x;
  npmPackages = pkgs.unstable.nodePackages_10_x;
  node2nixPackages = import ../../packages/node2nix/default.nix {
    pkgs = pkgs.unstable;
    nodejs = node;
  };
in
{ environment.systemPackages = (with pkgs.unstable; [
    node
  ] ++ (
    if stdenv.isDarwin
    then
    [
    ]
    else
    [
    # npm install may use any of these
    binutils
    gcc
    gnumake
    python2
    ]
  )) ++ (with npmPackages; [
    node-gyp
    node-gyp-build
    node-pre-gyp

    tern
    node2nix
    nodemon
    javascript-typescript-langserver
    typescript-language-server
    vscode-css-languageserver-bin
    vscode-html-languageserver-bin
    csslint
    eslint_d
    prettier
    typescript

    node2nixPackages.bunyan
    node2nixPackages.pino-pretty
    node2nixPackages."pnpm-3.6.2"
    node2nixPackages.prettier_d
    node2nixPackages.dockerfile-language-server-nodejs
    node2nixPackages.yaml-language-server
  ]);
}
