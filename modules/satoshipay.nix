{ config, pkgs, fetchurl, lib, ... }:

{ virtualisation = {
  docker = let
    daemonConfig = {
      ipv6 = true;
      fixed-cidr-v6 = "fd69:2074:9fcd:b0fd::/64";
      features = {
        buildkit = true;
      };
    };
    in {
      enable = true;
      enableOnBoot = false;
      liveRestore = false;

      extraOptions = "--config-file=${pkgs.writeText "daemon.json" (builtins.toJSON daemonConfig)}";

      autoPrune = {
        enable = true;
      };
    };
  };

  nixpkgs.config.allowUnfree = true;

  environment.variables = {
    KUBECTX_IGNORE_FZF = "1";
  };
  environment.systemPackages = with pkgs; [
    caddy
    openssl
    mongodb-tools
    pgadmin
    pgcli
    s3cmd
    sops
    unstable.mkcert
    unstable.google-cloud-sdk
    unstable.docker_compose
    unstable.kubernetes
    unstable.kubectx
    unstable.redis-desktop-manager
    unstable.kubernetes-helm
    unstable.helmfile
    unstable.robo3t
    unstable.slack
  ];

  services.mongodb = {
    enable = true;
    replSetName = "rs0";
    dbpath = "/tmp/mongodb";
  };
  systemd.services.mongodb.wantedBy = lib.mkForce  [];
  systemd.timers.mongodb = {
    description = "Delayed startup of MongoDB";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "1 min";
    };
  };
  systemd.services.mongodb-init = {
    description = "Init mongodb replicaset";
    requires = [ "mongodb.service" ];
    script = "${pkgs.mongodb}/bin/mongo --eval 'rs.initiate()'";
  };
  systemd.timers.mongodb-init = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "2 min";
    };
  };

  services.redis = {
    enable = true;
  };
  systemd.services.redis.wantedBy = lib.mkForce [];
  systemd.timers.redis = {
    description = "Delayed startup of Redis";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnActiveSec = "1 min";
    };
  };


  services.printing.drivers = with pkgs; [
  ];

  networking.domain = "satoshipay.io";

  networking.extraHosts = ''
    127.0.0.1 blogger.local wallet.satoshipay.local api.satoshipay.local ws.satoshipay.local
  '';
}
