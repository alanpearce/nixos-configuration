{ config, pkgs, ... }:

{ boot.loader.timeout = 1;

  environment.systemPackages = with pkgs; [
    nix-index
  ];

  networking.extraHosts = ''
    127.0.0.1 ${config.networking.hostName}
    ::1 ${config.networking.hostName}
  '';
}
