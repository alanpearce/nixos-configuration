{ config, pkgs, ... }:

let emacsPackage = import ./packages/emacs.nix {
  pkgs = pkgs.unstable;
  emacs = pkgs.unstable.emacs;
};
in
{
  imports = [
    ./private/default.nix

    ./modules/development/base.nix
    ./modules/development/javascript.nix
    ./modules/programs/accounting.nix
    ./modules/programs/dotfiles.nix
    ./modules/programs/shell.nix
  ];

  nixpkgs.config = {
    allowUnfree = true;
    packageOverrides = pkgs: {
      unstable = import <nixpkgs-unstable> {
        config = config.nixpkgs.config;
      };
    };
  };

  networking = {
    hostName = "trillian";
    knownNetworkServices = [ "Wi-Fi" "USB 10/100/1000 LAN" ];
    dns = [
      "::1"
      "127.0.0.1"
    ];
  };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep  wget
  environment.systemPackages = with pkgs;
    [
      mosh
      emacsPackage
      aspell
      aspellDicts.en
      darwin-zsh-completions
    ];

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  services.nix-daemon.enableSocketListener = true;
  # nix.package = pkgs.nix;

  services.activate-system.enable = true;

  # programs.nix-index.enable = true;

  programs.vim = {
    enable = true;
    enableSensible = true;
  };

  programs.zsh.enable = true;
  programs.zsh.enableBashCompletion = true;

  environment.variables.LANG = "en_GB.UTF-8";

  programs.gnupg = {
    agent = {
      enable = true;
      enableSSHSupport = false;
    };
  };

  # launchd.user.agents.emacs-daemon = {
  #   command = "${emacsPackage}/bin/emacs --daemon";
  #   serviceConfig = {
  #     KeepAlive = true;
  #   };
  # };
  #
  nix.gc = {
    automatic = true;
    options = "--max-freed $((25 * 1024**3 - 1024 * $(df -P -k /nix/store | tail -n 1 | awk '{ print $4 }')))";
  };
  nix.daemonNiceLevel = 10;
  nix.daemonIONice = true;

  nixpkgs.overlays = [
    (self: super: {
      darwin-zsh-completions = super.runCommandNoCC "darwin-zsh-completions-0.0.0"
        { preferLocalBuild = true; }
        ''
          mkdir -p $out/share/zsh/site-functions
          cat <<-'EOF' > $out/share/zsh/site-functions/_darwin-rebuild
          #compdef darwin-rebuild
          #autoload
          _nix-common-options
          local -a _1st_arguments
          _1st_arguments=(
            'switch:Build, activate, and update the current generation'\
            'build:Build without activating or updating the current generation'\
            'check:Build and run the activation sanity checks'\
            'changelog:Show most recent entries in the changelog'\
          )
          _arguments \
            '--list-generations[Print a list of all generations in the active profile]'\
            '--rollback[Roll back to the previous configuration]'\
            {--switch-generation,-G}'[Activate specified generation]'\
            '(--profile-name -p)'{--profile-name,-p}'[Profile to use to track current and previous system configurations]:Profile:_nix_profiles'\
            '1:: :->subcmds' && return 0
          case $state in
            subcmds)
              _describe -t commands 'darwin-rebuild subcommands' _1st_arguments
            ;;
          esac
          EOF
        '';
      })
  ];
  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;

  # You should generally set this to the total number of logical cores in your system.
  # $ sysctl -n hw.ncpu
  nix.maxJobs = 4;
  nix.buildCores = 2;
}
# vim: sw=2 sts=2 expandtab autoindent smarttab
