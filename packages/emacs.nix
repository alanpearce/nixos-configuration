{
  pkgs ? import <nixpkgs-unstable> {},
  emacs ? pkgs.emacs
}:

let
  myEmacs = pkgs.lib.overrideDerivation (emacs) (attrs: {
    postInstall = attrs.postInstall + ''
      rm $out/share/applications/emacs.desktop
    '';
  });
  stdenv = pkgs.stdenv;

  emacsWithPackages = (pkgs.emacsPackagesNgGen myEmacs).emacsWithPackages;
in
  emacsWithPackages (epkgs: (with epkgs; [
    ace-link
    all-the-icons
    almost-mono-themes
    add-node-modules-path
    ag
    all-the-icons
    amx
    auto-async-byte-compile
    avy
    basic-theme
    bash-completion
    caddyfile-mode
    company
    company-web
    company-tabnine
    counsel
    counsel-projectile
    crux
    docker-compose-mode
    dockerfile-mode
    editorconfig
    eink-theme
    eldoc-box
    emmet-mode
    esh-autosuggest
    esh-buf-stack
    esh-help
    eshell-fringe-status
    eshell-toggle
    eshell-up
    evil
    evil-collection
    evil-commentary
    evil-magit
    evil-mu4e
    evil-org
    evil-quickscope
    evil-space
    evil-surround
    eyebrowse
    feature-mode
    fish-completion
    flycheck
    flymake-diagnostic-at-point
    general
    git-gutter-fringe
    git-messenger
    git-timemachine
    gitattributes-mode
    gitconfig-mode
    gitignore-mode
    gitlab-ci-mode
    gitlab-ci-mode-flycheck
    goto-chg
    haskell-mode
    helpful
    ivy-hydra
    jinja2-mode
    js2-mode
    json-mode
    k8s-mode
    # kubernetes
    # kubernetes-evil
    ledger-mode
    lsp-mode
    lsp-ui
    lsp-haskell
    lsp-treemacs
    magit
    markdown-mode
    minions
    monotropic-theme
    moody
    nginx-mode
    nix-mode
    nix-update
    org-journal
    paredit
    php-mode
    posframe
    prettier-js
    projectile
    projectile-ripgrep
    quickrun
    relative-buffers
    restclient
    ripgrep
    rjsx-mode
    scss-mode
    spacemacs-theme
    swiper
    toml-mode
    typescript-mode
    undo-tree
    use-package
    web-mode
    wgrep-ag
    ws-butler
    which-key
    yaml-mode
  ] ++ (if stdenv.isDarwin then [
    exec-path-from-shell
    ns-auto-titlebar
  ] else [
  ])) ++ (with epkgs.elpaPackages; [
    rainbow-mode
  ]) ++ (if !stdenv.isDarwin then [
    # From main packages set

    pkgs.unstable.mu
  ] else []))
